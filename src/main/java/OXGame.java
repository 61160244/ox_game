
import java.util.Scanner;

public class OXGame {

    static int Player, row, column, countTurn = 0;
    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static Scanner kb = new Scanner(System.in);

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showSelectPlayer() {
        System.out.println("Select first player");
        System.out.println("    1:X   2:O");

    }

    public static void setPlayer(int player) {
        Player = player;
    }

    public static int getPlayer() {
        return Player;
    }

    public static void inputFirstPlayer() {
        setPlayer(kb.nextInt());
    }

    public static void showTable(char[][] table) {
        for (int i = 0; i <= 2; i++) {
            for (int j = 0; j <= 2; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println(" ");
        }
    }

    public static void showTurn(int Player) {
        if (Player == 1) {
            System.out.println("X Turn");
        } else if (Player == 2) {
            System.out.println("O Turn");
        } else {
            System.out.println("No have this player!! Please try again.");
        }
    }

    public static boolean checkPosition(int row, int column, char[][] table) {

        if (row > 3 || row < 1 || column > 3 || column < 1) {
            System.out.println("No has this position!! Please try again.");
            return false;
        } else if (table[row - 1][column - 1] != '-') {
            System.out.println("This position isn't empty!! Please try again.");
            return false;
        } else {
            return true;
        }
    }

    public static void main(String[] args) {

    }
}
